using System;
using System.Collections.Generic;
using Xunit;
using static Program;


namespace XUnitTestProject
{
    public class UnitTest1
    {
        [Fact]
        public void DeserializeTest()
        {
            Deserialize("{\"key1\": 1,\"key2\": {\"key3\": 1,\"key4\": {\"key5\": 4 }}}");
        }

        [Fact]
        public void PrintTest()
        {
            var xx = Deserialize("{\"key1\": 1,\"key2\": {\"key3\": 1,\"key4\": {\"key5\": 4 }}}");
            foreach (var item in xx)
            {
                Print(item);
            }
        }

        [Fact]
        public void LcaTest()
        {
            BinaryTree tree = new BinaryTree();
            tree.root = new Node(1);
            tree.root.left = new Node(2);
            tree.root.right = new Node(3);
            tree.root.left.left = new Node(4);
            tree.root.left.right = new Node(5);
            tree.root.right.left = new Node(6);
            tree.root.right.right = new Node(7);

            Node lca = tree.findLCA(6, 7);
        }
    }
}
