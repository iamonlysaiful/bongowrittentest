﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

public class Program
{
    private static int rownum = 0;
    public static void Main()
    {
        #region First solution


        Console.WriteLine("=======================First solution===============================================");
        var obj = Deserialize(JsonFile.Text);
        foreach (KeyValuePair<string, object> item in obj)
        {
            Print(item);
        }


        #endregion  

        #region second solution
        Console.WriteLine("=======================Second solution===============================================");
        rownum = 0;
        Person person_a = new Person("User", "1", "none");
        Person person_b = new Person("User", "1", person_a);



        var person_bSerializedJson = JsonConvert.SerializeObject(person_b);

        var json = JsonFileOfSecondQuestion(person_bSerializedJson);

        var obj2nd = Deserialize(json);

        foreach (KeyValuePair<string, object> item2nd in obj2nd)
        {
            Print(item2nd);
        }



        #endregion

        #region Third solution
        Console.WriteLine("=======================Third solution===============================================");
        BinaryTree tree = new BinaryTree();
        tree.root = new Node(1);
        tree.root.left = new Node(2);
        tree.root.right = new Node(3);
        tree.root.left.left = new Node(4);
        tree.root.left.right = new Node(5);
        tree.root.right.left = new Node(6);
        tree.root.right.right = new Node(7);

        Node lca = tree.findLCA(6, 7);
        if (lca != null)
        {
            Console.WriteLine("LCA(6, 7) = " + lca.data);
        }
        else
        {
            Console.WriteLine("Keys are not present");
        }
        #endregion 

        Console.ReadLine();

    }

    #region Given Json File of First Solution
    public static class JsonFile
    {
        public static string Text = "{\"key1\": 1,\"key2\": {\"key3\": 1,\"key4\": {\"key5\": 4 }}}";
    }
    #endregion

    #region first & second 
    public static Dictionary<string, object> Deserialize(string toDeserial)
    {
        rownum += 1;
        Dictionary<string, object> obj = JsonConvert.DeserializeObject<Dictionary<string, object>>(toDeserial);
        return obj;

    }

    public static void Print(KeyValuePair<string, object> obj)
    {

        try
        {


            int test = Convert.ToInt32(obj.Value);

            Console.WriteLine(string.Format("{0}  {1}", obj.Key, rownum));

        }
        catch (Exception)
        {
            string strInput = obj.Value.ToString().Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                   (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                Console.WriteLine(string.Format("{0}  {1}", obj.Key, rownum));
                var objdx = Deserialize(obj.Value.ToString());
                foreach (var itm in objdx)
                {
                    Print(itm);
                }
            }
            else
            {

                Console.WriteLine(string.Format("{0}  {1}", obj.Key, rownum));
            }



        }

    }
    #endregion

    #region  Given Json For second question
    public static string JsonFileOfSecondQuestion(string user)
    {
        return "{\"key1\": 1,\"key2\": {\"key3\": 1,\"key4\": {\"key5\": 4,\"user\":" + user + "}}}";
    }
    #endregion 

    #region Binary Tree Configuration For Third Question solution
    public class Node
    {
        public int data;
        public Node left, right;

        public Node(int item)
        {
            data = item;
            left = right = null;
        }
    }

    public class BinaryTree
    {
        // Root of the Binary Tree  
        public Node root;
        public static bool v1 = false, v2 = false;

        public virtual Node findLCAUtil(Node node, int n1, int n2)
        {

            if (node == null)
            {
                return null;
            }

            Node temp = null;

            if (node.data == n1)
            {
                v1 = true;
                temp = node;
            }
            if (node.data == n2)
            {
                v2 = true;
                temp = node;
            }

            // Look for keys in left and right subtrees  
            Node left_lca = findLCAUtil(node.left, n1, n2);
            Node right_lca = findLCAUtil(node.right, n1, n2);

            if (temp != null)
            {
                return temp;
            }


            if (left_lca != null && right_lca != null)
            {
                return node;
            }


            return (left_lca != null) ? left_lca : right_lca;
        }

        // Finds lca of n1 and n2 under the subtree rooted with 'node'  
        public virtual Node findLCA(int n1, int n2)
        {

            v1 = false;
            v2 = false;

            Node lca = findLCAUtil(root, n1, n2);

            if (v1 && v2)
            {
                return lca;
            }

            return null;
        }


    }
    #endregion

    #region Person Class of second solution
    [Serializable]
    class Person
    {
        [JsonProperty]
        string first_name, last_name;
        [JsonProperty]
        object father;

        [JsonConstructor]
        public Person(string first_name, string last_name, object father)
        {
            this.first_name = first_name;
            this.last_name = last_name;
            this.father = father;
        }

    }
    #endregion

}





